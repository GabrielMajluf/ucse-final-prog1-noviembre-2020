﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Usuario
    {
        public int Dni { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Telefono { get; set; }
        public string Direccion { get; set; }
        public Localidad Localidad { get; set; }
        public int CodigoPiletaInstalada { get; set; }
        public DateTime FechaInstalacion { get; set; }
        public DateTime FechaNacimiento { get; set; }
        
        public virtual string ObtenerDetalle()
        {
            return Apellido + Nombre + "Telefono:" + Telefono.ToString() + Localidad.Provincia + Localidad.Nombre + Direccion;
        }

        public abstract string FestejarAño();
    }    
}
