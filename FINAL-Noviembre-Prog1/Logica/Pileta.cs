﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Pileta
    {
        public int CodigoPileta { get; set; }
        public float CantidadLitros { get; set; }
        public float Precio { get; set; }
        //Corrección: No es un enumerador como se pide
        public int Color { get; set; } // 1 = Azul, 2 = Celeste, 3 = Gris
        public float Profundidad { get; set; }

        //Corrección: si esto fuera virtual y devolvería siempre un 0, solo sería necesario sobreescribir el método en las subclases que lo necesiten y no en todas.
        public abstract int DescontarPorcentaje();
    }
}
