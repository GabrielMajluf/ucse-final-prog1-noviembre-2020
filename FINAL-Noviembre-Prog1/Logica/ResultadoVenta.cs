﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class ResultadoVenta
    {
        public string Mensaje { get; set; }
        public bool Vendido { get; set; }

        public ResultadoVenta(bool pVendido, string pMensaje)
        {
            Mensaje = pMensaje;
            Vendido = pVendido;            
        }
    }
}
