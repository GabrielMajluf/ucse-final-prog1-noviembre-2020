﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class PiletaLona : Pileta
    {
        public decimal Alto { get; set; }
        public decimal Ancho { get; set; }
        public bool Filtro { get; set; } //True, tiene filtro.
        public bool CubrePiletas { get; set; } //True, tiene Cubre Piletas.

        public override int DescontarPorcentaje()
        {
            return 8;
        }
    }
}
