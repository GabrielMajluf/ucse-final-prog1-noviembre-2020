﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Cliente : Usuario
    {
        public override string ObtenerDetalle()
        {
            return base.ObtenerDetalle();
        }
        public override string FestejarAño()
        {
            var Diferencia = DateTime.Today - FechaInstalacion;

            //Corrección: ¿Que pasa con los años bisiestos?
            if (Diferencia.TotalDays == 365)
            {
                return "¡Feliitaciones por su primer aniversario de instalación!";
            }
            return null;
        }
    }
}
