﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class PiletaMaterial : Pileta
    {
        public decimal Largo { get; set; }
        public decimal Ancho { get; set; }   
        public bool Trampolin { get; set; } //True, tiene trampolin.
        public int CantidadEscaleras { get; set; }

        public override int DescontarPorcentaje()
        {
            if ((Profundidad >= 1.5) & (Trampolin = true))
            {
                return 10;
            }
            else
            {
                return 5;
            }
        }
    }
}
