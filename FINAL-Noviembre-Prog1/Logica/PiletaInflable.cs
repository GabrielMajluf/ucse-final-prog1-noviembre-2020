﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class PiletaInflable : Pileta
    {
        public decimal Diametro { get; set; }
        public bool CubrePiletas { get; set; } //True, tiene Cubre Piletas.

        public override int DescontarPorcentaje()
        {
            return 0;
        }
    }
}
