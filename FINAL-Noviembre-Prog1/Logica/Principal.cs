﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        List<Usuario> ListaUsuarios = new List<Usuario>();
        List<Venta> ListaVentas = new List<Venta>();
        List<Pileta> ListaPiletas = new List<Pileta>();

        public ResultadoVenta RegistrarVenta(int pDni, int pCodPileta)
        { 
            var BuscarUsuario = ListaUsuarios.Find(x => x.Dni == pDni);
            var BuscarPileta = ListaPiletas.Find(x => x.CodigoPileta == pCodPileta);
            string Mensaje = "";
            double ImporteTotal = BuscarPileta.Precio;
            bool Vendido = true;

            if (BuscarUsuario is null)
            {
                Cliente NuevoCliente = new Cliente();
                Usuario NuevoUsuario = NuevoCliente as Usuario;

                NuevoCliente.Dni = pDni;
                NuevoCliente.FechaInstalacion = DateTime.Today;
                NuevoCliente.CodigoPiletaInstalada = pCodPileta;
  
                ListaUsuarios.Add(NuevoUsuario);

                Mensaje = "Se dió de alta la venta y el cliente, recuerde completar los datos";
            }
            if (BuscarUsuario is Empleado)
            {
                var descuentoPorcentaje = BuscarPileta.DescontarPorcentaje();

                ImporteTotal = ((BuscarPileta.Precio * descuentoPorcentaje) / 100) + BuscarPileta.Precio;

                Mensaje = "El registro se realizó correctamente";
            }
            if (BuscarUsuario is Cliente)
            {
                Cliente NuevoCliente = BuscarUsuario as Cliente;
                
                NuevoCliente.CodigoPiletaInstalada = pCodPileta;
                NuevoCliente.FechaInstalacion = DateTime.Today;

                Mensaje = "Se registró la nueva pileta del cliente";
            }
            
            Venta nuevaVenta = new Venta (pDni, ImporteTotal);
            ListaVentas.Add(nuevaVenta);
            ResultadoVenta nuevoResultado = new ResultadoVenta (Vendido, Mensaje);

            return nuevoResultado;
        }
    }
}
