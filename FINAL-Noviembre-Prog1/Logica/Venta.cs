﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Venta
    {
        public int NumeroVenta { get; set; }
        public int DniCliente { get; set; }
        public DateTime FechaVenta { get; set; }
        public double ImporteTotal { get; set; }

        public Venta(int pDni, double pImportetotal)
        {
            //Corrección: esto no es correcto.
            NumeroVenta += 1;
            DniCliente = pDni;
            FechaVenta = DateTime.Today;
            ImporteTotal = pImportetotal;
        }
    }
}
