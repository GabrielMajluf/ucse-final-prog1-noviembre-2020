﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Empleado : Usuario
    {
        //Corrección: No es un enumerador como se pide
        public int Turno { get; set; } // 1 = Mañana, 2 = Tarde
        //Corrección: No es un enumerador como se pide
        public int AreaTrabajo { get; set; } // 1 = Gerencia, 2 = Ventas, 3 = Instalación 

        public override string ObtenerDetalle()
        {
            return base.ObtenerDetalle() + "Turno:" + Turno.ToString() + "Area:" + AreaTrabajo.ToString();
        }

        public override string FestejarAño()
        {
            //Corrección: No funciona esta validación.
            var Hoy = DateTime.Today.Day + DateTime.Today.Month;
            var Cumple = FechaNacimiento.Day + FechaNacimiento.Month;

            if (Hoy == Cumple)
            {
                return "¡Feliz cumpleaños compañero!";
            }
            return null;
        }
    }
}
