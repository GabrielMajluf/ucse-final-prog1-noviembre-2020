1) ¿Cuál es la diferencia entre sobreescritura y sobrecarga de métodos? 
La sobrecarga en C# permite sobrecargar o implementar un método, constructor o instancia con el mismo nombre, pero que reciba distintos argumentos.
El método sobrecarga solo puede sobrescribir un método virtual, si esta explicito que este método puede hacer la sobrescritura.
La sobrescritura u Override en C#, es necesario para ampliar o modificar la implementacion abstracta o virtual de un método  propiedad o evento heredado.


2) La afirmación correcta es:
a. El nivel de acceso protected restringe el acceso únicamente al código de la clase donde está definida, así como también al código de las clases que hereden de esta. CORRECTA.
b. El nivel de acceso public restringe el acceso únicamente al código de la clase donde está definida, así como a las de todas las clases que hereden de esta.
c. El nivel de acceso protected restringe el acceso a la variable únicamente al código de la clase donde está definida.


3) Explique similitudes y diferencias de los ciclos for y foreach.
La principal diferencia entre un foreach y un for es que el primero se utiliza para recorrer elementos dentro de una estructura o "arreglo" mientras que el for es una construcción que dice "realizar esta operación determinada cantidad de veces". (PODRÍA RECORRER UNA ESTRUCTURA CON UN FOR COMÚN)
La similitud es que ambas estructuras son iterativas. Lo que nos permite realizar una misma instrucción en reiteradas veces sin tener que repetir código.

BIEN.

4) Explique cuál es el objetivo principal de usar interfaces
Si distintas clases representan dos implementaciones de la misma funcionalidad, deberiamos poder usar cualquiera de las dos indistintamente. 
La funcionalidad (la interfaz) es la misma, lo que varía es la implementación. Es por ello que en programación orientada a objetos decimos que las interfaces son funcionalidades (o comportamientos) y las clases representan implementaciones.
Usar interfaces permite realizar polimorfismo y agregar nuevas clases de forma mucho más fácil (sólo debemos modificar donde instanciamos los objetos pero el resto de código queda igual)

OK.

5)Igual al 3.


6) Qué son y para qué se usan los parámetros por referencia?
Se usan anteponiendo la palabra reservada “ref” antes del tipo de datos del parámetro.
Su uso más importante es que como estamos apuntando a una dirección de memoria compartida entre parámetro formal y real, cuando modificamos el valor del parámetro dentro de un método, al salir, la variable que usamos en el parámetro también se modificará.
A efectos prácticos, podemos usar parámetros por referencia por ejemplo cuando cargamos una matriz, para que luego de terminar el procedimiento, tengamos la matriz actualizada


7) Marque la opción incorrecta
int[] numeros = new int[];
int[] numeros = new int[](1,2,3); ESTA ES LA INCORRECTA.
int[] numeros = new int[5];
int[] numeros = new int[] { 1,2,3,4,5 } ;


8) Explique qué son los parámetros formales y parámetros reales.
Parámetros reales: Las variables o los valores pasados ​​al llamar a una función se denominan parámetros reales.
Parámetros formales: Estas son las variables escritas o declaradas en la definición de la función, y reciben sus valores cuando se realiza una llamada a esa función.


9) ¿Qué son los subprogramas en NET? ¿Cómo se denominan? Qué tipos hay y cuales son sus similitudes y diferencias.
En .NET es costumbre llamarle “métodos” a todos los subprogramas, aunque teóricamente podemos dividirlo en:
- Procedimientos: Subprogramas que realizan un procesamiento y no retornan resultado.
- Funciones: Subprogramas que realizan un procesamiento y retornan resultados. 
La diferencia fundamental es que las funciones retornan el resultado de una operación a quien haya realizado la 
invocación, mientras que los procedimientos no retornan resultados, solo procesan.
La similitud que tienen es que nos permiten evitar codigo repetido, acomodarlo, hacerlo mas performante. Además dividen al programa en subprogramas lo que hace que sean mas comprensibles.
 
BIEN.
 
10) Explique brevemente qué es polimorfismo por herencia y por abstracción?
Polimorfismo significa varias formas diferentes. Es otro de los conceptos esenciales de la programación orientada a objetos. 
Hablamos de polimorfismo cuando definimos diferentes clases que tienen métodos implementados de diferentes maneras.
Tipos de polimorfismo
- Polimorfismo por herencia: Nos permite cambiar el tipo de un objeto para “transformarlo” en un objeto de la superclase.
- Polimorfismo por abstracción:  Por ejemplo, tenemos una clase Empleado y otra clase Cliente las cuales heredan de una clase abstracta Persona que tiene el método Hablar.
En las clases hijas sobreescribimos el método Hablar devolviendo el nombre de la persona y si es un empleado o un cliente.
En vez de tener un arreglo de empleados y otro de clientes por separado, usaremos el polimorfismo con un solo arreglo de personas, al cual agregaremos un alumno y un empleado para luego iterar y llamar al método Hablar.
A pesar de que tengamos un único arreglo de objetos de tipo persona, cuando llamamos a Hablar(), los objetos se comportan cliente o empleado.

MUY BIEN, APORTAN LAS CUESTIONES PRÁCTICAS PERO FALTÓ FUNDAMENTO TEÓRICO.
